# DESCRIPTION
#   Simple IP v4 address scanner.
#
# AUTHOR
#   Alan Noble (anoble@gmail.com)
#
import sys
import subprocess
import optparse
import re

DEBUG = True
USAGE = "ip_scanner.py -n a.b.c.d/e"

WINDOWS = True
if WINDOWS:
  PING = 'ping -n 1'
else:
  PING = 'ping -c 1'


def dotted_quad_to_int(dotted_quad):
  """Convert a dotted quad string to an integer."""
  quad = dotted_quad.split('.')
  return (int(quad[0]) << 24) + (int(quad[1]) << 16) + (int(quad[2]) << 8) + int(quad[3])

def int_to_dotted_quad(num):
  """Convert an integer to a dotted quad string."""
  quad = [str((num >> ii) & 0xFF) for ii in [24, 16, 8, 0]]
  return '.'.join(quad)

def ping(ip_address):
  """Ping an IP address by calling the ping program and return 0 on success."""
  if DEBUG: print "Pinging %s" % ip_address
  try:
    # we pipe the ping output to supress it; we only need the return code
    if WINDOWS: 
      output = subprocess.call(PING + ' ' + ip_address, shell=True)
      retcode = -1
    else:
      retcode = subprocess.call(PING + ' ' + ip_address, shell=True, stdout=subprocess.PIPE)
      if retcode < 0:
        if DEBUG: print >>sys.stderr, "Child was terminated by signal", -retcode
      else:
        if DEBUG: print >>sys.stderr, "Child returned", retcode
  except OSError, e:
    retcode = -1
    if DEBUG: print >>sys.stderr, "Execution failed:", e
  return retcode

def scan(network, start):
  """Scan a network, or a subnet"""

  rr = re.compile(r'(\d+\.\d+\.\d+\.\d+)\/(\d+)')
  mm = rr.match(network)
  if not mm:
    print >>sys.stderr, "Error: %s is not a valid network, e.g. 192.168.1.0/24" % network
    return

  addr = dotted_quad_to_int(mm.group(1))
  mask_bits = int(mm.group(2))
  net_bits = 32 - mask_bits
  net_mask = 0xFFFFFFFF << net_bits
  net_size = 1 << net_bits
  net_address = addr & net_mask

  if DEBUG:
    print "Net address: %s" % int_to_dotted_quad(net_address)
    print "Net mask: %s" % int_to_dotted_quad(net_mask)
    print "Net size: %d" % net_size
  
  print "Scanning network %s (%d possible hosts)" % (network, net_size - 1)
  print "(scanning from %s to %s)" % (int_to_dotted_quad(addr + start), int_to_dotted_quad(addr + net_size - 1))

  for num in range(start, net_size - 1):
    ip_address = int_to_dotted_quad(net_address + num)
    retcode = ping(ip_address)
    if retcode == 0:
      print ip_address + ' responded'
  
def main():
  global DEBUG
  p = optparse.OptionParser(usage=USAGE)
  p.add_option('--debug', '-d', action='store_true', default=False)
  p.add_option('--network', '-n', default='192.168.1.0/24')
  p.add_option('--start', '-s', default='1')
  
  options, arguments = p.parse_args()
  DEBUG = options.debug
  scan(options.network, int(options.start))

if __name__ == '__main__':
  main()
