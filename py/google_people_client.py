# Slice and dice contacts using the Google People API.
# Requires credentials.json
# Author: anoble@gmail.com
# See https://developers.google.com/people/api/rest/v1/people/
#
# Example usage:
# Find media contacts:
#  google-people-client.py -s "Media"
# Find media or writer contacts:
#  google-people-client.py -s "Media+W"
# Find female (F) Googlers (G):
#  google-people-client.py -s "F&G"
# Find female (F) writers (W):
#  google-people-client.py -s "F&W"
# Find female (F) friends of Susie (S) not in the USA:
#  google-people-client.py -s "F&S-US"
#
# Generate labels file first by running -g (groups) option with -j (json) option.

import os.path
import optparse
import sys
import time
import json

from io import BytesIO
from tokenize import tokenize, STRING, NAME, OP

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials

# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/contacts.readonly']

CREDENTIALS_FILE = os.path.join(os.path.expanduser('~'), '.config', 'google_credentials.json')
LABELS_FILE = os.path.join(os.path.expanduser('~'), '.config', 'contact_labels.json')
RATELIMIT_COUNT = 60 # Maximum number of successive API requests.
RATELIMIT_PAUSE = 60 # Seconds to pause when count is reached.

def authenticate():
  """Authenicate the user and return the creds."""
  
  creds = None
  # The file token.json stores the user's access and refresh tokens, and is
  # created automatically when the authorization flow completes for the first
  # time.
  if os.path.exists('token.json'):
    creds = Credentials.from_authorized_user_file('token.json', SCOPES)

  # If there are no (valid) credentials available, let the user log in.
  if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
      creds.refresh(Request())
    else:
      if not os.path.exists(CREDENTIALS_FILE):
        sys.exit("credentials missing: %s" % CREDENTIALS_FILE)
      flow = InstalledAppFlow.from_client_secrets_file(CREDENTIALS_FILE, SCOPES)
      creds = flow.run_local_server(port=0)

    # Save the credentials for the next run
    with open('token.json', 'w') as token:
      token.write(creds.to_json())
  return creds

def getConnections(service, n):
  """Prints the name of the first n connections."""
    
  print("List %d connection names" % n)
  results = service.people().connections().list(
        resourceName='people/me',
        pageSize=n,
        personFields='names,emailAddresses').execute()
  connections = results.get('connections', [])

  for person in connections:
    names = person.get('names', [])
    if names:
      name = names[0].get('displayName')
      print(name)
      print(names[0])

def getGroups(service, jsn=False, n=1000):
  """Prints first n contact groups (labels)."""
    
  if jsn:
    print("{")
  results = service.contactGroups().list(
    pageSize=n
  ).execute()
  groups = results.get('contactGroups', [])

  lines = []
  for grp in groups:
    name = grp.get('name')
    # resourceName is 'contactGroups/id'.
    id = grp.get('resourceName')[len('contactGroups/'):]
    count = grp.get('memberCount', 0)
    if jsn:
      if name == '♀':
        name = 'F'
      lines.append('"%s": "%s"' % (name, id))
    else:
      print("%s %s: %d members" % (id, name, count))
  if jsn:
    print(',\n'.join(lines))
    print("}")

def getGroupMembers(service, group, n=1000):
  """Get first n members of a contact group (label)."""
    
  results = service.contactGroups().get(
    resourceName='contactGroups/'+group,
    maxMembers=n
  ).execute()

  return results.get('memberResourceNames', [])

def printMember(service, name):
  result = service.people().get(
    resourceName=name,
    personFields='names,emailAddresses,organizations,biographies'
  ).execute()
  
  names = result.get('names')
  emails = result.get('emailAddresses')
  orgs = result.get('organizations')
  notes = result.get('biographies')
  if names:
    print("%s %s" % (names[0].get('givenName'), names[0].get('familyName')), end='')
    if orgs:
      print(" (%s)" % orgs[0].get('name'), end='')
    if emails:
      for email in emails:
        print(", %s" % email.get('value'), end='')
    print()
    if notes:
      print("[%s]" % notes[0].get('value'))
    print()

def getSpec(service, spec):
  """Get a set of labels according to the given logical spec. Operators
are + (union), & (intersect), and - (remove). For example, A&B-C denotes the
intersection set of A and B excluding C."""

  # Read the labels
  if not os.path.exists(LABELS_FILE):
    sys.exit("labels missing: %s" % LABELS_FILE)
  with open(LABELS_FILE) as f:
    labels = json.load(f)

  json.loads
  # Tokenize the spec.
  # NB: tokenize.tokenize expects a readable object.
  tokens = tokenize(BytesIO(spec.encode('utf-8')).readline)

  # Evaluate the spec.
  s = set()
  op = '+'
  for toknum, tokval, _, _, _ in tokens:
    if toknum == NAME:
      id = labels.get(tokval)
      if not id:
        sys.exit("label %s not found" % tokval)
      l = getGroupMembers(service, id)
      if op == '+':
        s = s.union(set(l))
      elif op == '&':
        s = s & set(l)
      elif op == '-':
        s = s - set(l)

    if toknum == OP:
      if tokval not in ['+', '&', '-']:
        sys.exit("invalid op %s" % tokval)
      op = tokval

  print("%s: %d" % (spec, len(s)))
  count = 0
  for name in s:
    count += 1
    printMember(service, name)
    if count % RATELIMIT_COUNT == 0:
      print("Pausing...")
      time.sleep(RATELIMIT_PAUSE)

def init():
  pass

def main():
  p = optparse.OptionParser()
  p.add_option('--init', '-i')
  p.add_option('--spec', '-s')
  p.add_option('--json', '-j', action='store_true')
  p.add_option('--groups', '-g', action='store_true')
  p.add_option('--labels', '-l', action='store_true')

  options, arguments = p.parse_args()
  creds = authenticate()
  service = build('people', 'v1', credentials=creds)

  if options.init:
    init()

  if options.groups:
    getGroups(service, options.json)

  if options.spec:
    getSpec(service, options.spec)

main()

