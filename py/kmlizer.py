# DESCRIPTION
#   Various KML routines
#
# AUTHOR
#   Alan Noble (anoble@gmail.com)
#
# REQUIRES
#   Python2.7
#
import sys
import os
import optparse
import math
import re
import sys

from latlng_utm_conversion import LLtoUTM, UTMtoLL
from xml.dom.minidom import parse, parseString

###########################
# Globals

USAGE="usage: kmlizer.py [options]"

ReferenceEllipsoid = 23

TestDoc = """<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://earth.google.com/kml/2.2"><Placemark><name>D50567/A23</name><Style><PolyStyle><fill>0</fill><outline>1</outline></PolyStyle></Style><description>D50567/A23</description><MultiGeometry><Point><coordinates>136.9635389,-35.6835401</coordinates></Point><Polygon><outerBoundaryIs><LinearRing><coordinates>136.9670186,-35.6756950 136.9666097,-35.6760904 136.9626171,-35.6763812 136.9605522,-35.6756424 136.9596928,-35.6753348 136.9594868,-35.6752611 136.9595145,-35.6760510 136.9596353,-35.6762787 136.9595681,-35.6765737 136.9593728,-35.6767827 136.9586957,-35.6771607 136.9585341,-35.6773973 136.9587166,-35.6774600 136.9590993,-35.6775922 136.9588112,-35.6795431 136.9591665,-35.6802904 136.9572269,-35.6825222 136.9565444,-35.6841384 136.9559959,-35.6863625 136.9558136,-35.6870636 136.9567671,-35.6879129 136.9573237,-35.6883913 136.9578856,-35.6888493 136.9587647,-35.6899219 136.9602209,-35.6918191 136.9604852,-35.6914407 136.9618580,-35.6903606 136.9619559,-35.6893442 136.9624059,-35.6881145 136.9620156,-35.6866860 136.9629503,-35.6859134 136.9635954,-35.6841175 136.9637081,-35.6826791 136.9639637,-35.6819740 136.9665320,-35.6816512 136.9679304,-35.6817974 136.9686894,-35.6813872 136.9709639,-35.6807397 136.9710765,-35.6804393 136.9712642,-35.6802848 136.9710141,-35.6797405 136.9711462,-35.6761189 136.9670186,-35.6756950</coordinates></LinearRing></outerBoundaryIs></Polygon></MultiGeometry></Placemark></kml>"""

TestDoc2 = """<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://earth.google.com/kml/2.2"><Placemark><name><![CDATA[Track 1]]></name><description><![CDATA[]]></description><styleUrl>#track</styleUrl><MultiGeometry><LineString><coordinates>148.735554,-20.241166,58.0 148.736423,-20.240721,53.0 148.737298,-20.240351,53.0 148.738161,-20.239884,53.0 </coordinates></LineString></MultiGeometry></Placemark></kml>"""

###########################
# Objects

class UtmPoint(object):
  """UTM coordinate (x, y) and optional zone."""
  def __init__(self, x, y, zone=0):
    self.coords = (x, y)
    self.zone = zone # UTM zone
  def __add__(self, other):
    coords = (a + b for a, b in zip(self.coords, other.coords))
    pt = UtmPoint(*coords)
    pt.zone = self.zone
    return pt
  def __sub__(self, other):
    coords = (a - b for a, b in zip(self.coords, other.coords))
    pt = UtmPoint(*coords)
    pt.zone = self.zone
    return pt
  def __mul__(self, other):
    pt = UtmPoint(*(a * other for a in self.coords))
    pt.zone = self.zone
    return pt
  __rmul__ = __mul__
  def __div__(self, other):
    pt = UtmPoint(*[a / other for a in self.coords])
    pt.zone = self.zone
    return pt
  def __str__(self):
    return 'UtmPoint' + str(tuple(self.coords))
  __repr__ = __str__
  def __getitem__(self, index):
    return self.coords[index]

  def toLatLon(self):
    (lat, lon) = UTMtoLL(ReferenceEllipsoid, self.coords[1], self.coords[0], self.zone)
    return LatLon(lon, lat)
    
  def distanceFrom(self, other):
    dx = self.coords[0] - other.coords[0]
    dy = self.coords[1] - other.coords[1]
    return math.sqrt(dx * dx + dy * dy)
    
class LatLon(object):
  """Lattitude-Longitude coordinate.  If lat is missing, lon is expected to be a comma-separated 'lon,lat' string."""
  def __init__(self, lon, lat=None):
    if lat is None:
      lonlat = lon.split(',')
      if len(lonlat) < 2: sys.exit("LatLon(%s) invalid" % lon)
      self.lat = float(lonlat[1])
      self.lon = float(lonlat[0])
    else:
      self.lat = float(lat)
      self.lon = float(lon)

  def __str__(self):
    return "%.6f,%.6f" % (self.lat, self.lon)
  
  def toUtmPoint(self):
    """Convert to Point in UTM coordinates."""
    (zone, easting, northing) = LLtoUTM(ReferenceEllipsoid, self.lat, self.lon)
    return UtmPoint(easting, northing, zone)
      
###########################

def ScalePoly(scale, poly, center=None):
  if not poly:
    return poly
  if not center:
    center = poly[0] * 0.
  return [scale * (pt - center) + center for pt in poly]

def Center(poly):
  ctr = sum(poly, 0. * poly[0]) / len(poly)
  ctr.zone = poly[0].zone
  return ctr

def Polygon(*coords):
  return [UtmPoint(*c) for c in coords]

def Coords(poly):
  return [p.coords for p in poly]

def SignedArea_(a, b, c):
  """Compute area of one trapezoid."""
  ba = b - a
  ca = c - a
  return 0.5 * (ba[0]*ca[1] - ca[0]*ba[1])

def Area(poly):
  """Calculate area of polygon, using the sum of trapezoids method."""
  if len(poly) < 3:
    return 0.
  a = poly[0]
  area = sum([SignedArea_(a, b, c) for b, c in zip(poly[1:-1], poly[2:])])
  if area < 0:
    area = -area
  return area

def parsePolygonCoordinates(xml_string, xml_file=None):
  """Return list of polygon coordinates as LatLons."""

  if xml_file:
    dom = parse(xml_file)
  else:
    dom = parseString(xml_string)
  kml_element = dom.documentElement
  assert kml_element.tagName == "kml"

  polygon_list = kml_element.getElementsByTagName("Polygon")
  results = []
  for polygon_element in polygon_list:
    nodelist = polygon_element.getElementsByTagName("coordinates")
    coordinates_element = nodelist.item(0)
    coordinates_data = coordinates_element.firstChild.data

    results.append([LatLon(coord) for coord in coordinates_data.split()])

  dom.unlink()
  return results

###########################
# polygon routines

def polygonsFromFile(kml_file):
  """Return list of polygons (list of lists) in kml file."""
  # extract latlon coordinates
  results = []
  polys = parsePolygonCoordinates('', kml_file)
  for poly in polys:
    # convert latlons to polygon of UTM points
    results.append([latlon.toUtmPoint() for latlon in poly])
  return results

def polygonAreaFromFiles(files):
  total = 0.0
  for file in files:
    polys = polygonsFromFile(file)
    count = 0
    for poly in polys:
      count += 1
      area = Area(poly)
      total += area
      print "%s-%d: %.2f Ha (%.2f acres) (%d points)" % (file, count, (area/10000), (area*2.47105381/10000), len(poly))
  print "Total %.3f Ha, %.3f acres, %d m^2" % (total/10000, total*2.47105381/10000, total)

def scalePolygonFromFile(file, scale, centerLatLon):
  pt = centerLatLon.toUtmPoint()
  polys = polygonsFromFile(file)
  for poly in polys:
    scaledPoly = ScalePoly(scale, poly, pt)

    latlon_coords = [pt.toLatLon() for pt in scaledPoly]
    for ll in latlon_coords: print str(ll),

###########################
# line routines

def parseLineCoordinates(xml_string, xml_file=None):
  """Returns single list of all LineString coordinates as LatLons."""
  if xml_file:
    dom = parse(xml_file)
  else:
    dom = parseString(xml_string)
  kml_element = dom.documentElement
  assert kml_element.tagName == "kml"

  linestring_list = kml_element.getElementsByTagName("LineString")
  results = []
  for linestring_element in linestring_list:
    nodelist = linestring_element.getElementsByTagName("coordinates")
    coordinates_element = nodelist.item(0)
    coordinates_data = coordinates_element.firstChild.data

    for coord in coordinates_data.split():
      results.append(LatLon(coord))

  dom.unlink()
  return results

def lineLength(latlon_coords, prev_point = None):
  total = 0
  point = None
  for latlon in latlon_coords:
    point = latlon.toUtmPoint()
    if prev_point is not None:
      total += point.distanceFrom(prev_point)
    prev_point = point
  return (total, point)

def lineLengthFromFiles(files):
  total = 0.0
  prev_point = None
  for file in files:
    latlon_coords = parseLineCoordinates(None, file)
    (subtotal, prev_point) = lineLength(latlon_coords, None)
    print "File %s subtotal %d m, %d km, %d nm" % (file, subtotal, subtotal/1000, subtotal/1852)
    total += subtotal        
  print "Total %d m, %d km, %d nm" % (total, total/1000, total/1852)

###########################
# conversion routines

def convertUtmToLatLon(utm_coords):
  """Convert comma-separated UTM coords to lat,lon, e.g., 55K,681291.164699,7760880.73775 => -20.2411659992,148.735554"""
  [zone, easting, northing] = utm_coords.split(',')
  (lat, lon) = UTMtoLL(ReferenceEllipsoid, float(northing), float(easting), zone)
  print "%s => %.6f,%.6f" % (utm_coords, float(lat), float(lon))
  
def convertLatLonToUtm(latlon_string):
  """Convert comma-separated lat,lon to UTM coords, e.g., -20.2411659992,148.735554 => 55K,681291.164699,7760880.73775"""
  (lat, lon) = latlon_string.split(',')
  lat = float(lat)
  lon = float(lon)
  (zone, easting, northing) = LLtoUTM(ReferenceEllipsoid, lat, lon)
  print "%s => %s,%s,%s" % (latlon_string, zone, easting, northing)
  
###########################

def unitTest():

  origin = UtmPoint(0.,0.)
  triangle = Polygon((1.,2.),(2.,3.),(0.,3.))

  print "triangle: ", triangle
  print origin
  print triangle

  center = Center(triangle)

  area_scale = 3.
  length_scale = math.sqrt(area_scale)

  tripled_triangle = ScalePoly(length_scale, triangle, center)

  print tripled_triangle

  print "Area(triangle): ", Area(triangle)
  print "Area(tripled_triangle): ", Area(tripled_triangle)

  square = Polygon((0,0),(1,0),(1,1),(0,1))

  print Area(square)
  print Coords(square)

  polys = parsePolygonCoordinates(TestDoc)
  latlon_coords = polys[0]
  ll = latlon_coords[0]
  (zone, easting, northing) = LLtoUTM(ReferenceEllipsoid, ll.lat, ll.lon)
  print zone, easting, northing
  (lat, lon) = UTMtoLL(ReferenceEllipsoid, northing, easting, zone)
  print lat, lon

  # length test
  latlon_coords = parseLineCoordinates(TestDoc2)
  (distance, prev_point) = lineLength(latlon_coords)
  assert(int(distance) == 307)
  start = latlon_coords[0]
  finish = prev_point.toLatLon()
  print "Distance from %s to %s = %d m" % (str(start), str(finish), distance)

  # conversion
  convertLatLonToUtm("-20.241166,148.735554")
  convertUtmToLatLon("55K,681291.164699,7760880.73775")
  
###########################
def main():
  p = optparse.OptionParser(usage=USAGE)
  p.add_option('--test', '-t', action='store_true')
  p.add_option('--area', '-a', action='store_true')
  p.add_option('--length', '-l', action='store_true')
  p.add_option('--scale', '-s') # float
  p.add_option('--utm', '-u')  # utm => latlon
  p.add_option('--zone', '-z')  # latlon => utm
  p.add_option('--ellipsoid', '-e')
  p.add_option('--center', '-c', default='-35.432701,139.166565')
  
  options, arguments = p.parse_args()

  if options.ellipsoid:
    ReferenceEllipsoid = options.ellipsoid

  if options.test:
    return unitTest()
    
  if options.utm:
    return convertUtmToLatLon(options.utm)
    
  if options.zone:
    return convertLatLonToUtm(options.zone)

  files = arguments
    
  if options.area:
    polygonAreaFromFiles(files)
    
  elif options.length:
    lineLengthFromFiles(files)
    
  elif options.scale:
    ll = options.center.split(',')
    latlon = LatLon(ll[1], ll[0])
    scalePolygonFromFile(files, float(options.scale), latlon)
    
if __name__ == '__main__':
  main()

