import sys

BIDIR = True   # True if network costs are bidirectional.    
DEBUG = False  # True to debug.
TIME = 0       # Simulation "time", incremented with each pass.
POISON = False # True if Poisoned Reverse.

def printCosts(x, y, v, x_y, x_v, v_y):
  """Print costs from x to y directly and via v and True if the cost via
v is different from the current cost, or False otherwise."""
  print("x=%s, y=%s, v=%s" % (x, y, v))
  print("  x_v=%s + v_y=%s != x_y=%s" % (x_v, v_y, x_y), end=' ')
  if x_y < 0 or x_v < 0 or v_y < 0:
    print("Invalid!")
  else:
    print(x_v + v_y != x_y)

def copyTable(orig):
  """Return a copy of the given routing table."""
  table = {}
  for y in orig:
    table[y] = {}
    for v in orig:
      table[y][v] = orig[y][v]
  return table
    
def poisonTable(orig, v):
  """Returns a poisoned copy of the supplied routing table, as
follows. If there is a lower-cost route through another node v to get
to a destination y, then advertise that our distance to y is
infinity."""
  poisoned = False
  table = copyTable(orig)
  for y in orig:
    if v == y:
      continue
    if orig[y][v] != -1 and orig[y][v] < orig[y][y]:
      if DEBUG: print("Poisoning route to %s" % y)
      # Advertise our distance to y is infinity for all routes.
      for u in orig:
        table[y][u] = float('inf')
      poisoned = True
  return table, poisoned
  
class Node:
  """Represents one node in the network."""
  def __init__(self, name, destinations):
    """Construct node."""
    if DEBUG: print("Node %s instantiated" % name)
    self.name = name
    self.destinations = destinations
    self.others = []
    self.table = {}
    # Initialize list of other nodes.
    self.destinations.sort()
    for y in self.destinations:
      if y != self.name:
        self.others.append(y)

  def init(self, edges, partial):
    """Initialize a node's routing table given the supplied edges. If
partial if True, perform a partial update of the routing
table. Table[y][v] holds the distance (cost) for destination y via hop
v. Table[y][y] is the distance to y if this node is directly connected
to y, or -1 if this is node is not connected to y directly."""
    if DEBUG: print("Node %s initialized with: %s" % (self.name, edges))

    if partial:
      # Update table for only supplied edges.
      if DEBUG:
        print("Partially updating ", self.name, " with ", edges)
      for v in edges:
        if edges[v] != -1:
          delta = edges[v] - self.table[v][v] # Compute the cost change.
          # Adjust costs for all destinations y via v.
          for y in self.others:
            self.table[y][v] += delta
        else:
          # All y are unreachable via v.
          for y in self.others:
            self.table[y][v] = -1
      return

    # Initialize the routing table (a dict of dicts)
    for y in self.others:
      # y is the destination
      self.table[y] = {}
      for v in self.others:
        # v is the first hop
        self.table[y][v] = float('inf')

    # Populate known distances, i.e., distances to our neighbors.
    for v in self.others:
      if v in edges and edges[v] != -1:
        self.table[v][v] = edges[v]
      else:
        # All y are unreachable via v.
        for y in self.others:
          self.table[y][v] = -1

  def neighbors(self, nodes):
    """Return a node's neighbors. These are nodes for which table[y][y] is not -1."""
    neighbors = []
    for y in self.others:
      if self.table[y][y] != -1:
        # Add node y to neighbors.
        for nd in nodes:
          if y == nd.name:
            neighbors.append(nd)
    if DEBUG:
      print("Node %s has neighbors %s" % (self.name, [n.name for n in neighbors]))
    return neighbors
  
  def send(self, table, recipients):
    """Send a node's routing table to the given recipients, returning the
 set of names of the recipients that changed or the empty set if no
 recipients changed. Recipients is typically the node's neighbors."""
    if DEBUG: print("Node %s is sending to %s: %s" %
                    (self.name, [r.name for r in recipients], table))

    changed = set() # The set of names of recipients that changed.
    for r in recipients:
      if POISON:
        t, poisoned = poisonTable(table, r.name)
        if DEBUG and poisoned: print("Node %s is poisoning %s" % (self.name, r.name))
      else:
        t = table
      if r.receive(self.name, t):
        if DEBUG: print("Recipient %s changed" % r.name)
        changed.add(r.name)
      else:
        if DEBUG: print("Recipient %s unchanged" % r.name)
    return changed

  def receive(self, other, table):
    """Receive a table and update accordingly. Return true if changed, false otherwise."""
    if DEBUG: print("Node %s is receiving a table from %s: %s" % (self.name, other, table))

    changed = False
    
    # My distance to other.
    t = self.table.get(other)
    if t is None:
      print("ERROR: Got message from unknown node" % other)
      return
    x_v = t[other]
    if x_v == -1:
      if DEBUG: print("Ignoring message from disconnected node ", other)
      return False

    # For y in destinations: D_x(y) = min_v{c(x,v) + D_v(y)}.
    for y in self.others:
      x_y = self.table[y][other] # My distance to y via other.
      
      y_distances = table.get(y) # Other's distances to y.
      if not y_distances:
        continue
      # Calculate min_v{c(x,v)}
      v_y = float('inf')
      for v in y_distances:
        if y_distances[v] != -1 and y_distances[v] < v_y:
          v_y = y_distances[v]

      if DEBUG: printCosts(self.name, y, other, x_y, x_v, v_y)
      if x_y != x_v + v_y:
        self.table[y][other] = x_v + v_y
        changed = True

    return changed
    
  def printTable(self):
    """Print the distance table the desired format. The major axis is the
destination and the minor axis is the first hop, e.g, table[y][v]
holds the distance to destination y via hop v."""
    print("router %s at t=%d" % (self.name, TIME))

    # Print others.
    print("  ", end='')
    for y in self.others:
      print("%3s" % y, end=' ')
    print()
    
    # For each y print distances via hop v.
    for y in self.others:
      print(y, end=' ')
      for v in self.others:
        d = self.table[y][v]
        if d == -1:
          print("  -", end=' ')
        else:
          print("%3s" % str(d).upper(), end=' ')
      print()
    print()
      
  def printRoutes(self):
    """Print converged routes, i.e., the mininum-cost route for each
destination, or print 'unreachable' otherwise."""

    for y in self.others:
      y_distances = self.table.get(y)
      hop = None
      cost = float('inf')
      for v in y_distances:
        if y_distances[v] < 0:
          continue
        if y_distances[v] < cost:
          cost = y_distances[v]
          hop = v
      if hop is not None:
        print("router %s: %s is %s routing through %s" % (self.name, y, cost, hop))
      else:
        print("router %s: %s is unreachable" % (self.name, y))

def makeBidirectional(nodes, edges):
  """Make the network bidirectional given only edges in one
direction. In other words, make edges[u][v] the same as
edges[v][u]."""
  for u in nodes:
    for v in edges[u]:
      edges[v][u] = edges[u][v]
  return edges

def simulate(destinations, edges, nodes):
  """Simulate a network with the given destinations, edges and nodes. If
nodes is None, then instantiate and initialize them, else use the
existing nodes."""
  global TIME
  
  targets = []    # Nodes that require updates, initally all nodes.

  if DEBUG:
    print("Simulating with names ", destinations, " and edges ", edges)

  if nodes is None:
    # Instantiate the nodes in our network.
    nodes = []
    for n in destinations:
      nodes.append(Node(n, destinations))

    # Initialize each node's routing table and build up recipients and targets.
    # Targets will initially be all nodes in the network.
    for nd in nodes:
      targets.append(nd)
      nd.init(edges[nd.name], False) # Full initialization.

  else:
    # Use existing nodes.
    # Edges are only the edges that have changed since last time.
    # Targets will initially only be the nodes that have changed.
    for nd in nodes:
      if nd.name in edges:
        targets.append(nd)
        nd.init(edges[nd.name], True) # Partial initialization.

  # Keep sending to nodes until there are no more changes,
  # printing routing tables with each pass.
  while len(targets) > 0:
    if DEBUG: print("t=", TIME)
    # Make a copy of each table before each pass to simulate the same
    # table being sent to each recipient.
    tables = dict()
    for nd in nodes:
      nd.printTable()
      tables[nd.name] = copyTable(nd.table)
    # Each target node sends its table to it neighbors.
    changed = set()
    for nd in targets:
      changed.update(nd.send(tables[nd.name], nd.neighbors(nodes)))
    # Send only to nodes that have changed.
    if DEBUG: print("Changed: ", changed)
    targets = []
    for nd in nodes:
      if nd.name in changed: 
        targets.append(nd)
    TIME += 1

  # Print converged routes.
  for nd in nodes:
    nd.printRoutes()
  print()

  return nodes
    
##################################################
# Built-in tests.

def test1():
  """Example of 3-node network described in assignment without a change."""
  names = [
    'X',
    'Y',
    'Z',
  ]

  edges = {
    'X':{ 'Y': 2, 'Z': 7 },
    'Y':{ 'Z': 1 },
    'Z':{}
  }

  print("test: bidirectional 3-node network")
  edges = makeBidirectional(names, edges)
  nodes = simulate(names, edges, None)

def test2():
  """4-node network. See https://en.wikipedia.org/wiki/Distance-vector_routing_protocol"""
  names = [
    'A',
    'B',
    'C',
    'D',
  ]

  edges = {
    'A':{ 'B': 3, 'C': 23 },
    'B':{ 'C': 2 },
    'C':{ 'D': 5},
    'D':{}
  }

  print("test2: bidirectional 4-node network")
  edges = makeBidirectional(names, edges)
  simulate(names, edges, None)

def test3():
  """"5-node network. See https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm."""
  names = [
    'S',
    'V1',
    'V2',
    'V3',
    'V4'
  ]

  edges = {
    'S': { 'V1': 9, 'V2': 3 },
    'V1':{ 'V2': 6, 'V4': 2 },
    'V2':{ 'V1': 2, 'V3': 1 },
    'V3':{ 'V2': 2, 'V4': 2 },
    'V4':{}
  }

  print("test3: undirectional 5-node network")
  simulate(names, edges, None)

def test4():
  """Example of 3-node network described in assignment with a network change."""
  names = [
    'X',
    'Y',
    'Z',
  ]

  edges = {
    'X':{ 'Y': 2, 'Z': 7 },
    'Y':{ 'Z': 1 },
    'Z':{}
  }

  print("test4: bidirectional 3-node network with a change")
  edges = makeBidirectional(names, edges)
  nodes = simulate(names, edges, None)

  # Make some changes.
  edges = {
    'X':{},
    'Y':{},
    'Z':{}
  }
  edges['X']['Z'] = 5
  edges['Z']['X'] = 5
  edges['Y']['Z'] = -1
  edges['Z']['Y'] = -1
  nodes = simulate(names, edges, nodes)

def test5():
  """Test poisoning"""
  global DEBUG
  DEBUG = True
  
  print("test5: test poisoning")
  # A cannot route to C at a lower cost via B.
  # A can route to B at a lower cost via C than directly.
  orig = {'B':{'B':4, 'C':2},
          'C':{'B':5, 'C':1}}
  print("Original:      ", end=' ')
  print(orig)
  table, poisoned = poisonTable(orig, 'B')
  if poisoned:
    print("Poisoned for B:", end=' ')
    print(table)
  else:
    print("Not poisoned for B")
  table, poisoned = poisonTable(orig, 'C')
  if poisoned:
    print("Poisoned for C:", end=' ')
    print(table)
  else:
    print("Not poisoned for C")
  print()
  
  # Similar to above but with a third node, D.
  # A can route to B at a lower cost via D than directly.
  orig = {'B':{'B':4, 'C':2, 'D':3},
          'C':{'B':5, 'C':1, 'D':-1},
          'D':{'B':5, 'C':-1,'D':2}}
  print("Original:      ", end=' ')
  print(orig)
  table, poisoned = poisonTable(orig, 'B')
  if poisoned:
    print("Poisoned for B:", end=' ')
    print(table)
  else:
    print("Not poisoned for B")
  table, poisoned = poisonTable(orig, 'C')
  if poisoned:
    print("Poisoned for C:", end=' ')
    print(table)
  else:
    print("Not poisoned for C")
  table, poisoned = poisonTable(orig, 'D')
  if poisoned:
    print("Poisoned for D:", end=' ')
    print(table)
  else:
    print("Not poisoned for D")

def test6():
  """4-node network with poisoning."""
  global POISON
  POISON = True

  names = [
    'A',
    'B',
    'C',
    'D',
  ]

  edges = {
    'A':{ 'B': 4, 'C': 1, 'D':2 },
    'B':{ 'A': 4, 'C': 1, 'D':1 },
    'C':{ 'A': 1, 'B': 1, 'D':5 },
    'D':{ 'A': 2, 'B': 1, 'C':5 }
  }
  
  print("test6: 4-node network with poisoning with a change")
  nodes = simulate(names, edges, None)

  # Sever links B-C and B-D. 
  edges['B']['C'] = -1
  edges['B']['D'] = -1
  edges['C']['B'] = -1
  edges['D']['B'] = -1
  simulate(names, edges, nodes)
  
##################################################
# Run built-in tests.
#test1()
#test2()
#test3()
#test4()
#test5()
#test6()

##################################################

def main():
  """Read network topology from stdin and run the simulation."""
  names = []
  edges = {}
  nodes = None
  
  naming = True
  blank = False
  for line in sys.stdin:
    line = line.rstrip()
    if naming:
      # We're expecting a name.
      if line == '':
        naming = False # No more names are expected.
        blank = True
        continue
      names.append(line)
      blank = False
      continue

    # Else we're expecting an edge.
    if line == '':
      if blank:
        exit(0) # Two blank lines in a row -> exit.
      blank = True
      nodes = simulate(names, edges, nodes)
      continue

    if blank:
      # Begining of topology section.
      # Create (empty) edges for each node.
      for x in names:
        edges[x] = {}
      
    blank = False    
    parts = line.split(' ')
    if len(parts) != 3:
      print("ERROR: edge requires 3 values: x y cost")
      exit(1)
    try:
      cost = int(parts[2])
    except:
      print("ERROR: cost must be an int")
      exit(1)
    x = parts[0]
    y = parts[1]  
    edges[x][y] = cost
    if BIDIR:
      edges[y][x] = cost

main()

