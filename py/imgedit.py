# DESCRIPTION
#   A script to edit JPEG images in various ways.
#   
#   Use cases include:
#   - Inverting a directory of images of scanned negatives.
#   - Setting sequential timestamps for images in a directory.
#   - Prefixing image files with YYYYMMDD extracted from the file's last-modified time.
#   - Reading and clearing EXIF data.
#
# USAGE
#   -i invert file(s).
#   -p prefix files(s) with YYYYMMDD from the last-modified time.
#   -t timestamp file(s) with given date in YYYY:MM:DD format, YYYY:MM:DD HH:MM:SS,
#      or using the last-modified time of the file if '-t modified'.
#      Automatic timestamps are incremented by one minute with each file.
#   -n when timestamping, offset from this value instead of zero.
#   -d process files in a directory.
#   -r read EXIF data
#   -c clear EXIF data
#   -j convert file to JPEG
#   
# AUTHOR
#   anoble@gmail.com
#
# REQUIREMENTS
#   Python 3, pillow, piexif

from PIL import Image
import PIL.ImageOps
from PIL.ExifTags import TAGS
import piexif
import sys
import optparse
import os
import datetime
import time
import re

USAGE = "imgedit [-i] [-p] [-c] [-r] [-n offset] [-d dir] [-t (YYYY:MM:DD( HH:MM:SS)?)|(modified)] [file1 file2...]"

def process(filename, newname, time, options):
  """If invert is true, invert filename and save to newname, optionally inserting time as the timestamp. Otherwise just insert the timestamp without inverting. If time is 'modified', use the last-modified time of the file for the image modified time."""
  
  base, ext = os.path.splitext(filename)
  if not options.jpeg and ext.upper() != '.JPG':
    print("Skipping non-JPEG file %s" % filename)
    return

  try:
    file = open(filename)
  except:
    sys.exit("Cannot open file " + filename)
  try:
    img = Image.open(filename)
  except:
    sys.exit("ERROR: Cannot open image " + filename)

  if options.jpeg:
    img.convert('RGB').save(newname, quality=95)
    return

  if options.clear:
    # Clear EXIF data from the image.
    data = list(img.getdata())
    newImg = Image.new(img.mode, img.size)
    newImg.putdata(data)
    newImg.save(newname)
    print("%s => %s without EXIF" % (filename, newname))
    return
    
  # Get original EXIF info if any.
  img_exif = img.info.get("exif")

  exif_dict = {}
  if img_exif: 
    exif_dict = piexif.load(img_exif)

  if options.read:
    print("%s has EXIF data: %s" % (filename, str(exif_dict)))

  if not (options.invert or options.time or options.prefix):
    return
  
  if options.invert:
    img = PIL.ImageOps.invert(img)

  if options.time:
    if len(time) == 10: # Only YYYY:MM:DD was supplied.
      time += " 00:00:00" # Append hh:mm:ss

    # Set new date/time.
    exif = exif_dict.get("Exif")
    if not exif:
      exif_dict["Exif"] = {}
    zeroth = exif_dict.get("0th")
    if not zeroth:
      exif_dict["0th"] = {}
    exif_dict["Exif"][piexif.ExifIFD.DateTimeOriginal]=time
    exif_dict["0th"][piexif.ImageIFD.DateTime]=time
    exif_dict["thumbnail"] = None # Remove the thumbnail as we don't need it and it is often corrupt.
  
  exif_bytes = piexif.dump(exif_dict)

  if options.prefix:
    # Ignore newname and instead prefix files with YYYYMMDD or
    # 00000000 if no image time is found.
    dir = os.path.dirname(filename)
    if not os.path.exists(dir):
      os.mkdir(dir)
    pr = '00000000 ' 
    zeroth = exif_dict.get("0th")
    if zeroth:
      t = zeroth.get(piexif.ExifIFD.DateTimeOriginal)
      if not t:
        t = zeroth.get(piexif.ImageIFD.DateTime)
      if t:
        t = t.decode("utf-8")
        # Create a YYYYMMDD prefix from "YYYY:MM:DD".
        pr = t[0:4] + t[5:7] + t[8:10] + ' '
    newname = os.path.join(dir, pr + os.path.basename(filename))

  img.save(newname, "jpeg", exif=exif_bytes, optimize=True)
  if time:
    print("%s => %s @ %s" % (filename, newname, time))
  else:
    print("%s => %s" % (filename, newname))

def processDir(dirname, options):
  """Process each file in directory. When options.time is specified this
is used as the starting timestamp and is incremented per minute for
each file."""
  if not os.path.isdir(dirname):
    sys.exit("ERROR: '" + dirname + "' is not a directory")

  # Set defaults.
  hour = 12
  mins = 0
  mtime = False
  
  if options.time:
    if options.time == 'modified':
      mtime = True
    else:
      date = options.time[0:10] # Date is YYYY:MM:DD.
      if len(options.time) > 10:
        hour = int(options.time[11:13])
        mins = int(options.time[14:16])

  else:
    print("WARNING: date/time not specified")
  
  print("Processing files in '%s' with options %s\n" % (dirname, str(options)))

  with os.scandir(dirname) as entries:
    filenames = []
    for entry in entries:
      filenames.append(entry.name)
    filenames.sort()

    cnt = options.offset # Start counting from the supplied offset.
    mins += options.offset
    prev = None

    for filename in filenames:
      filename = os.path.join(dirname, filename)
      if mtime:
        ts = os.path.getmtime(filename)
        date = datetime.datetime.fromtimestamp(ts).strftime("%Y:%m:%d")
        if date != prev:
          prev = date
          # Reset hour and minutes.
          hour = 12
          mins = 0

      cnt += 1
      mins += 1
      if mins % 60 == 0:
        hour += 1
        mins = 0
        
      if options.time:
        time = "%s %02d:%02d:00" % (date, hour, mins)
      else:
        time = ''

      if mtime:
        newname = "%s %s #%02d.JPG" % (dirname, date.replace(':', ''), cnt)
      else:
        newname = "%s #%02d.JPG" % (dirname, cnt)
      process(filename, newname, time, options)

def main():
  p = optparse.OptionParser(usage=USAGE)
  p.add_option('-i', action="store_true", dest="invert")
  p.add_option('-j', action="store_true", dest="jpeg")
  p.add_option('-p', action="store_true", dest="prefix")
  p.add_option('-c', action="store_true", dest="clear")
  p.add_option('-r', action="store_true", dest="read")
  p.add_option('-d', default='', dest="dir")
  p.add_option('-t', default='', dest="time")
  p.add_option('-n', type="int", default=0, dest="offset")
  
  options, args = p.parse_args()

  if len(sys.argv) == 1:
    print(USAGE)
    sys.exit(1)
 
  if options.time:
    if options.time != "modified":
      m = re.match(r'^\d\d\d\d:\d\d:\d\d( \d\d:\d\d:\d\d)?$', options.time)
      if not m:
        print(USAGE)
        sys.exit(1)

  if options.dir:
    processDir(options.dir, options)
    return

  for filename in args:
    print("Processing %s with options %s\n" % (filename, str(options)))
    dir = os.path.dirname(filename)
    newname = os.path.join(dir, "_" + os.path.basename(filename))
    if options.jpeg:
      newname = newname.rpartition('.')[0] + '.jpg'
    process(filename, newname, options.time, options)

main()

