# Read a CSV file of contacts and extract the email addresses.

import sys 
import csv
import optparse

USAGE = "parse_contact_emails.py [--domain domain] --input csvfile"

EMAIL_COLUMN = 30 # The column containing the email address.

def main():
  op = optparse.OptionParser(usage=USAGE)
  op.add_option('--input', '-i', default="contacts.scv")
  op.add_option('--domain', '-d', default="")
  
  options, arguments = op.parse_args()

  with open(options.input) as csvfile:
    reader = csv.reader(csvfile)
    count = 0
    for row in reader:
      email = row[EMAIL_COLUMN]
      if not email:
        continue
      if options.domain and options.domain not in email:
        continue
      count += 1
      print("%s" % email)

  print("%d rows" % count)


main()
