# Generate times in which some digits are the same.
hours = [12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

def genTimes():
  for hr in hours:
    for min in range(60):
      time = "%02d%02d" % (hr, min)
      print time

def genTimesWithSameDigits(digCount):
  for hr in hours:
    for min in range(60):
      time = "%02d%02d" % (hr, min)
      for search_dg in [0, 1, 2, 3, 4, 5]:
        count = 0
        for pos in range(4):
          if int(time[pos]) == int(search_dg):
            count += 1
        if count >= digCount:
          print time


#genTimes()

print "2 digits same:"
genTimesWithSameDigits(2)
print "3 digits same:"
genTimesWithSameDigits(3)
print "4 digits same:"
genTimesWithSameDigits(4)
