# Attempt to resolve hostnames in the given domain.
import sys, socket

USAGE = "bulk_resolve domain hostlist_file"

if len(sys.argv) < 3:
        sys.exit(USAGE)
        
domain = sys.argv[1]
filename = sys.argv[2]

hostlist_file = open(filename,"r") #read dictionary file

for h in hostlist_file.readlines():
  h = h.strip() + "." + domain
  print("%s" % h)
  try:
    ip = socket.gethostbyname(h)
    print("%s resolves to %s" % (h, ip))
  except:
    pass # ignore error
            
