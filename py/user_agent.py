import http.client
import urllib.parse
import base64

host = 'nobletech.com'
url = '/'
auth = "Basic " + base64.b64encode(bytes("admin:password", 'utf-8')).decode('ascii')
data = {"submit":"Request Secret"}
params = urllib.parse.urlencode(data)

headers = {"Content-type": "application/x-www-form-urlencoded", "User-Agent":"Foo", "Authorization":auth}

connection = http.client.HTTPConnection(host, 80, timeout=10)
print(connection)
connection.request("POST", url, params, headers=headers)
response = connection.getresponse()
print("Status: {} and reason: {}".format(response.status, response.reason))
print(response.read())
connection.close()
