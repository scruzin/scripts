# Perform various tasks using the Google Drive API.
# Author: anoble@gmail.com
# See https://developers.google.com/drive/api/v3/about-sdk

import os.path
import optparse
import math

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from apiclient.http import MediaFileUpload

USAGE="usage: google-drive-client.py [--quickstart][---dir][--count]"

CREDENTIALS_FILE = os.path.join(os.path.expanduser('~'), 'google_credentials.json')
# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/drive']
ID = "19cYu_7-z3MyCZu_ZErTuOUmgOseBqNJJ" # Design Studio folder.
VERBOSE = False

GRADES = {
  0: 'unknown',
  1: 'fails to meet expectations',
  2: 'somewhat below expectations',
  3: 'meets expectations',
  4: 'exceeds expectations',
  5: 'strongly exceeds expectations'
}

def authenticate():
  """Authenicate the user and return the creds."""
  
  creds = None
  # The file token.json stores the user's access and refresh tokens, and is
  # created automatically when the authorization flow completes for the first
  # time.
  if os.path.exists('token.json'):
    creds = Credentials.from_authorized_user_file('token.json', SCOPES)

  # If there are no (valid) credentials available, let the user log in.
  if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
      creds.refresh(Request())
    else:
      if not os.path.exists(CREDENTIALS_FILE):
        sys.exit("credentials missing: %s" % CREDENTIALS_FILE)
      flow = InstalledAppFlow.from_client_secrets_file(CREDENTIALS_FILE, SCOPES)
      creds = flow.run_local_server(port=0)

    # Save the credentials for the next run
    with open('token.json', 'w') as token:
      token.write(creds.to_json())
  return creds

def quickstart(service):
  """Shows basic usage of the Drive v3 API.
  Prints the names and ids of the first 10 files the user has access to."""
  
  results = service.files().list(
    pageSize=10,
    fields="nextPageToken, files(id, name)"
  ).execute()
  items = results.get('files', [])
  if not items:
    print('No files found.')
  else:
    print('Quickstart files:')
    for item in items:
      print(u'{0} ({1})'.format(item['name'], item['id']))
          
def permCallback(request_id, response, exception):
  """Handle newly created permissions."""
  if exception:
    print(exception)
  else:
    print("Permission ID: %s" % response.get('id'))

def setPermissions(service, folders):
  print("Setting permissions for %d folders" % len(folders.keys()))
  batch = service.new_batch_http_request(callback=permCallback)

  for email in folders:
    fileId = folders[email]
    print("Sharing folder ID %s with %s" % (fileId, email))
    perm = {
      'type': 'user',
      'role': 'writer',
      'emailAddress': email,
      'sendNotificationEmail': True,
    }
    batch.add(service.permissions().create(
      fileId=fileId,
      body=perm,
      fields='id',
    ))

  batch.execute()

def makeDirs(service, dirsFile, parent):
  """Make subfolders using names in dirsFile under a folder with the ID parent.
The file format is name,email."""
  # First, create the subfolders.
  folders = {} # Contains file ID by owner's email address.
  
  f = open(dirsFile, 'r')
  dirNames = f.readlines()
  print("Making %d folders" % len(dirNames))
  f.close()

  for line in dirNames:
    line = line.strip()
    if line == "" or line[0] == '#':
      continue
    parts = line.split(",")
    name = parts[0]
    email = parts[1]
    metadata = {
      'name': name,
      'parents': [parent],
      'mimeType': 'application/vnd.google-apps.folder'
    }
    subdir = service.files().create(body=metadata, fields='id').execute()
    fileId = subdir.get('id')
    folders[email] = fileId
    print("Created folder %s with ID: %s" % (name, fileId))

  # Second, set the permissions
  setPermissions(service, folders)

def getFile(service, id):
  result = service.files().get(
    fileId=id,
  ).execute()
  return result
    
def countDir(service, folderID, level, list):
  """Count the files in a folder, recursing if level > 0."""
  
  # Query for files which include 'folderID' as a parent.
  q = "'" + folderID + "' in parents"
  
  page_token = None
  response = service.files().list(
    q=q,
    spaces='drive',
    fields='nextPageToken, files(id,name)',
    pageToken=page_token
  ).execute()

  files = response.get('files', [])
  count = len(files)

  for file in files:
    if list:
      print("%s,%s" % (file['id'], file['name']))
    if level > 0:
      subcount = countDir(service, file['id'], level-1, list)
      if VERBOSE and subcount == 0:
        print("* %s is empty!" % file['name'])
      count += subcount
      print()
      
  return count

def uploadFile(service, folderID, fileName, mimeType='text/csv'):
  """Upload a file."""
  metadata = {
    'name': fileName,
    'parents': [folderID]
   }
  media = MediaFileUpload(fileName,
                          mimetype=mimeType,
                          resumable=True)  
  f = service.files().create(body=metadata,
                             media_body=media,
                             fields='id').execute()
  print("File ID: %s" % f.get('id'))

def multiDir(service, multiDir):
  """Look for dirs.csv and data.csv under multiDir and upload to the given folder."""

  if VERBOSE:
    print("Reading from %s" % multiDir)
    
  # First, read the directories.
  dirsFile = os.path.join(multiDir, 'dirs.csv')
  f = open(dirsFile, 'r')
  lines = f.readlines()
  f.close()

  # Map student names to their respectie folder IDs.
  dirInfo = {}
  for line in lines:
    line = line.strip()
    if line == "" or line[0] == '#':
      continue
    parts = line.split(",")
    dirInfo[parts[1]] = parts[0]

  # Second, create file for each line in data.csv
  dataFile = os.path.join(multiDir, 'data.csv')
  f = open(dataFile, 'r')
  lines = f.readlines()
  f.close()

  count = 0
  for line in lines:
    line = line.strip()
    if line == "" or line[0] == '#':
      continue
    parts = line.split(",")
    # The first column is the student name
    name = parts[0]
    # The following is the data.
    # NB: Customize this as necessary.
    # Grades are on a scale of 1 to 5 from 4 people, so divide by 4 and round.
    a = GRADES[round(int(parts[1])/4)]
    b = GRADES[round(int(parts[2])/4)]
    c = GRADES[round(int(parts[3])/4)]
    d = GRADES[round(int(parts[4])/4)]

    # Create CSV file to upload.
    fileName = 'mid_review_feedback.csv'
    with open(fileName, 'w+') as f:
      f.write("Response to brief,%s\n" % a)
      f.write("Architectural expression,%s\n" % b)
      f.write("Ocean (AusOcean) theme,%s\n" % c)
      f.write("Convincing & implementable,%s" % d)
    # Upload the file.
    folderID = dirInfo.get(name)
    if not folderID:
      print("Name %s not found" % name)
      continue
      
    uploadFile(service, folderID, fileName, 'text/csv')
    print("%s uploaded" % name)
    count += 1
    os.remove(fileName)
  print("%d files uploaded" % count)
  
def main():
  global VERBOSE
  p = optparse.OptionParser(usage=USAGE)
  p.add_option('--quickstart', '-q', action='store_true')
  p.add_option('--dir', '-d')
  p.add_option('--count', '-c', action='store_true')
  p.add_option('--upload', '-u')
  p.add_option('--verbose', '-v', action='store_true')
  p.add_option('--level', '-l')
  p.add_option('--id', '-i')
  p.add_option('--multi', '-m')
  
  options, arguments = p.parse_args()

  creds = authenticate()
  service = build('drive', 'v3', credentials=creds)

  if options.quickstart:
    quickstart(service)

  VERBOSE = options.verbose
  
  if options.id:
    id = options.id
  else:
    id = ID

  if options.level:
    level = int(options.level)
  else:
    level = 0

  if options.dir:
    makeDirs(service, options.dir, id)

  if options.count:
    print("Count = %d" % countDir(service, id, level, True))

  if options.upload:
    uplodadFile(service, id, options.upload)

  if options.multi:
    multiDir(service, options.multi)

main()
