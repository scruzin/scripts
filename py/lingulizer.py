# DESCRIPTION
#   Analyze words in the International Phonetic Alphabet (IPA) from a file.
#   The file must be in UTF-8 format.
#   
# AUTHOR
#   anoble@gmail.com
#
# REQUIREMENTS
#   Python 3

import sys

USAGE = "lingulizer word_file"

# The universe of IPA vowels.
IPA_VOWELS = ['i', 'ɪ', 'e', 'ɛ', 'æ', 'a', 'ʌ', 'ə', 'u', 'ʊ', 'o', 'ɔ']
IPA_EXCLUDE = ["ˈ", "ː", "ˌ"] # Exclude stress indicators.

# Words and other relationships that we find.
WORDS = []
VOWELS = {}
VOWEL_POSITIONS = {}
CONSONANTS = {}
CONSONANT_POSITIONS = {}
CONSONANT_CLUSTER_WORDS = {}
CONSONANT_BEGIN_WORDS = {}
CONSONANT_END_WORDS = {}
CONSONANT_BEGIN_END_WORDS = {}
MINIMAL_PAIR_WORDS = {}
NEAR_MINIMAL_PAIR_WORDS = {}

# Array utilties.
def appendToDictArray(dct, key, val):
  """Append val as an array element to dictionary dct[key].""" 
  if key in dct:
    if val not in dct[key]:
      dct[key].append(val)
  else:
    dct[key] = [val]

def strArray(arr):
  return ", ".join(arr)

# String utilities.
def stripWord(word):
  """Remove IPA indicator charactors from from a word."""
  for c in IPA_EXCLUDE:
    word = word.replace(c, '')
  return word

def readWordsFromFile(filename):
  """Read words from filename and populate WORDS."""
  global WORDS
  
  try:
    file = open(filename, encoding="utf8")
  except:
    sys.exit("Cannot open " + filename)
      
  lines = file.readlines()
  
  # Strip delimeters and new lines.
  for line in lines:
    line = line.strip()
    line = line.strip("/[]")
    WORDS.append(line)

  print("Found %d words:" % len(WORDS))
  for word in WORDS:
    print(word)

def findVowelsAndConsonants():
  """Find vowels and consonants in WORDS populating VOWELS and CONSONANTS."""
  global VOWELS
  global CONSONANTS
  
  for word in WORDS:
    for p in word:
      if p in IPA_EXCLUDE:
        continue
      if p in IPA_VOWELS:
        # Save the words that contain each vowel phoneme.
        appendToDictArray(VOWELS, p, word)
          
      else: # Not a vowel, therefore a consonant.
        # Save the words that contain each consonant phoneme.
        appendToDictArray(CONSONANTS, p, word)

  print("\nFound %d vowels:" % len(VOWELS))
  for vowel in VOWELS:
    print(vowel + ": " + str(len(VOWELS[vowel])))
    
  print("\nWords by vowel:")
  for vowel in VOWELS:
    print(vowel + ": " + strArray(VOWELS[vowel]))

  print("\nFound %d consonants:" % len(CONSONANTS))
  for consonant in CONSONANTS:
    print(consonant + ": " + str(len(CONSONANTS[consonant])))
    
  print("\nWords by consonant:")
  for consonant in CONSONANTS:
    print(consonant + ": " + strArray(CONSONANTS[consonant]))
 
def findPositions(phonemes):
  """Find phoneme positions."""
  positions = {}
  
  for p in phonemes:
    for word in phonemes[p]:
      # Remove excluded IPA characters to form simplified word.
      sword = stripWord(word)

      # Find the position of this phoneme.
      pos = sword.index(p)
      if pos == 0:
        # At beginning.
        r = "#|%s" % sword[pos+1]
      elif pos == len(sword) - 1:
        # At end.
        r = "%s|#" % sword[pos-1]
      else:
        # In middle.
        r = "%s|%s" % (sword[pos-1], sword[pos+1])
      appendToDictArray(positions, p, r)

  return positions

def findVowelPatterns():
  """Find vowel patterns."""
  global VOWEL_POSITIONS
  
  # Find vowel positions.
  VOWEL_POSITIONS = findPositions(VOWELS)
  
  print("\nVowel positions (list form):")
  for vowel in VOWEL_POSITIONS:
    print(vowel + ": " + strArray(VOWEL_POSITIONS[vowel]))

  print("\nVowel positions (T-diagram form):")
  for vowel in VOWEL_POSITIONS:
    print("\n " + vowel)
    print("---")
    for r in VOWEL_POSITIONS[vowel]:
      print(r)

def findConsonantPatterns():
  """Find consonant patterns."""
  global CONSONANT_POSITIONS
  global CONSONANT_CLUSTER_WORDS
  global CONSONANT_BEGIN_WORDS
  global CONSONANT_END_WORDS
  global CONSONANT_BEGIN_END_WORDS

  # Find consonant positions.
  CONSONANT_POSITIONS = findPositions(CONSONANTS)
  
  print("\nConsonant positions (list form):")
  for consonant in CONSONANT_POSITIONS:
    print(consonant + ": " + strArray(CONSONANT_POSITIONS[consonant]))
  
  print("\nConsonant positions (T-diagram form):")
  for consonant in CONSONANT_POSITIONS:
    print("\n " + consonant)
    print("---")
    for r in CONSONANT_POSITIONS[consonant]:
      print(r)
  
  # Find consonant clusters.
  for word in WORDS:
    for i in range(len(word) - 1):
      if word[i] in CONSONANTS and word[i+1] in CONSONANTS:
        appendToDictArray(CONSONANT_CLUSTER_WORDS, word[i] + word[i+1], word)

  print("\nConsonant clusters:")
  for cc in CONSONANT_CLUSTER_WORDS:
    print(cc + ": " + strArray(CONSONANT_CLUSTER_WORDS[cc]))
  
  # Find constants at the beginning or ending of each word.
  for word in WORDS:
    sword = word
    for c in IPA_EXCLUDE:
      sword = sword.replace(c, '')
    b = sword[0]
    e = sword[-1]

    if b in CONSONANTS:
      appendToDictArray(CONSONANT_BEGIN_WORDS, b, word)
      if b == e:
        appendToDictArray(CONSONANT_BEGIN_END_WORDS, b, word)
    if e in CONSONANTS:
      appendToDictArray(CONSONANT_END_WORDS, e, word)

  print("\nConsonants at beginning:")
  for consonant in CONSONANT_BEGIN_WORDS:
    print(consonant + ": " + strArray(CONSONANT_BEGIN_WORDS[consonant]))
  
  print("\nConsonants at ending:")
  for consonant in CONSONANT_END_WORDS:
    print(consonant + ": " + strArray(CONSONANT_END_WORDS[consonant]))
  
  print("\nConsonants at both beginning and ending:")
  for consonant in CONSONANT_BEGIN_END_WORDS:
    print(consonant + ": " + strArray(CONSONANT_BEGIN_END_WORDS[consonant]))

def compareWords(word, words, maxdiff):
  """Compare words and return words that differ by maxdiff. Minimal
pairs differ by 1 and near minimal pairs differ by 2."""
  pairs = None
  sword = stripWord(word)
  for w in words:
    sw = stripWord(w)
    if sword == sw:
      continue
    diff = 0
    for i in range(len(sw)):
      if sw[i] == sword[i]:
        continue
      diff += 1
      if diff > maxdiff:
        break
    if diff == maxdiff:
      if not pairs:
        pairs = {}
      if word < w:
        pairs[word] = w
      else:
        pairs[w] = word
  return pairs
      
def findMinimalPairs():
  """ Find minimal pairs."""
  global MINIMAL_PAIR_WORDS
  global NEAR_MINIMAL_PAIR_WORDS

  # Words with minimal pairs must have the same stripped length.
  # Group stripped words by length.
  gwords = {}
  for w in WORDS:
    sword = stripWord(w)
    appendToDictArray(gwords, len(sword), w)

  # Compare all words for each length twice.
  # ToDo: Optimize.
  for l in gwords:
    if len(gwords[l]) < 2:
      continue # Nothing to compare with!
    for w in gwords[l]:
      pairs = compareWords(w, gwords[l], 1)
      if pairs:
        MINIMAL_PAIR_WORDS[l] = pairs
      pairs = compareWords(w, gwords[l], 2)
      if pairs:
        NEAR_MINIMAL_PAIR_WORDS[l] = pairs

  print("\nMinimal pairs:")
  for l in MINIMAL_PAIR_WORDS:
    for w in MINIMAL_PAIR_WORDS[l]:
      print(w + " " + MINIMAL_PAIR_WORDS[l][w])
  
  print("\nNear minimal pairs:")
  for l in NEAR_MINIMAL_PAIR_WORDS:
    for w in NEAR_MINIMAL_PAIR_WORDS[l]:
      print(w + " " + NEAR_MINIMAL_PAIR_WORDS[l][w])
  
def main():
  if len(sys.argv) < 1:
    sys.exit(USAGE)

  readWordsFromFile(sys.argv[1])
  findVowelsAndConsonants()
  findVowelPatterns()
  findConsonantPatterns()
  findMinimalPairs()
  # ToDo: Find complementary distributions.
  
main()
