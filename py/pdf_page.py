# DESCRIPTION
#   Eextract 1 page from a PDF doc.
#
# AUTHOR
#   Alan Noble (anoble@gmail.com)
import sys
import os
import optparse
from PyPDF2 import PdfFileReader, PdfFileWriter

USAGE = "pdf -f file -p pagenum"

def writeOnePage(page, pagename):
  # output 1 page
  output = PdfFileWriter()
  output.addPage(page)
  outputStream = open(pagename, "wb")
  output.write(outputStream)

def main():
  p = optparse.OptionParser(usage=USAGE)
  p.add_option('--file', '-f')
  p.add_option('--page', '-p')
  
  options, arguments = p.parse_args()

  if options.file:
    file = options.file
  else:
    file = "file.pdf"

  if options.page:
    pagenum = int(options.page)
  else:
    pagenum = 1
   
  try: 
    input = PdfFileReader(file, "rb")
  except:
    sys.exit("Cannot open %s" % file)

  # print # pages
  num_pages = input.getNumPages()
  print("%s has %d pages." % (file, num_pages))

  print("Extracting page %d" % pagenum)
  page = input.getPage(pagenum - 1)
  writeOnePage(page, "page_%d.pdf" % pagenum)

#text = page.extractText()+'\n' 
#clean_text  = " ".join(text.replace(u"\xa0", " ").strip().split())     
#print text

main()

