# Concatenate MPEG-TS files in urls.txt to out.ts

import requests

infilename = 'urls.txt'
outfilename = 'out.ts'

print("Reading URLs from %s" % infilename)
inf = open(infilename, 'r')
urls = [line.strip() for line in inf.readlines()]
inf.close()

outf = open(outfilename, 'ab')
for url in urls:
  print("getting %s" % url)
  r = requests.get(url) # ToDo: Check for errors.
  outf.write(r.content)

print("Wrote to %s" % outfilename)
outf.close()
