# Strip EXIF metadata from image.

from PIL import Image
import sys

def clear_exif(filename):
  try:
    file = open(filename)
  except:
    sys.exit("Cannot open file " + filename)
  try:
    image = Image.open(filename)
  except:
    sys.exit("Cannot open image " + filename)

  # Next 3 lines strip EXIF.
  data = list(image.getdata())
  image_without_exif = Image.new(image.mode, image.size)
  image_without_exif.putdata(data)

  # Save the file without EXIF.
  image_without_exif.save('_' + filename)

sys.argv.pop(0)
for filename in sys.argv:
  clear_exif(filename)


